import { useContext, useEffect} from 'react'

import UserContext from '../UserContext';

import { Navigate } from 'react-router-dom';


export default function Logout() {

	// consume the UserContext object and destructure it to access the user state and unsetUser function from context provider.
	const { unsetUser, setUser } = useContext(UserContext);

		//localStorage.clear()
	// CLear localStorage of the user's information
	unsetUser();

	useEffect(() =>{
		setUser({id: null});
	})

	return (

		<Navigate to="/login" />
	)
};