
import { useState, useEffect,useContext, axios } from 'react';

import {Navigate, useNavigate, Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import { Form, Button, Table } from 'react-bootstrap';

import ReactivateProductButton from '../components/DeactivateAndReactivate'

import Swal from 'sweetalert2';

import CreateProductView from '../components/CreateProductView';
import UpdateProductView from '../components/UpdateProductView';


	export default function AdminRole() {
		

	 const [selectedProductId, setSelectedProductId] = useState(null);



	const { user } = useContext(UserContext);

	const navigate = useNavigate();
	
	const [products, setProducts] = useState([]);

	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState(true);
	const [productId, setProductId] = useState("");



	/*// Function to create new product
	const createProduct = (e) => {
    	e.preventDefault();
    	// Check if user is admin
    	if (user.isAdmin) {
        	fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
           	 method: "POST",
            	headers: {
            		'Content-Type': 'application/json',
            		Authorization: `Bearer ${localStorage.getItem('token')}`
            	},
           		body: JSON.stringify({
            		title: title,
           	 		description: description,
            		price: price,
            		isActive: isActive,
           			productId: productId
           		 })
            })
            .then(res => res.json())
            .then(data => {
            	console.log(data);
             if(data !== null){
                    // Clear input fields
                    setTitle("");
                    setDescription("");
                    setPrice("");

                    Swal.fire({
                      title: "Product created successfully",
                      icon: "success",
                      text: "Your product has been created and is now available for purchase."
                    })

                    navigate("/products")

                  } else {
                    Swal.fire({
                      title: "Error creating product",
                      icon: "error",
                      text: "There was an error creating your product. Please try again."
                    })
                  }
                })
            }
        }    
*/
        const activeProduct = (e) => {
            e.preventDefault();
            // Check if user is admin
            if (user.isAdmin) {
            	fetch(`${process.env.REACT_APP_API_URL}/products/activeProduct`, {
            	headers: {
            	Authorization: `Bearer ${localStorage.getItem('token')}`
            		}
            	})
            	.then(res => res.json())
            	.then(data => {
           			 console.log(data);
            		setProducts(data);
           		 })
     
            } 
          };


          const updateProduct = (productId) => {
             setSelectedProductId(productId);
             // navigate to the update product view
             navigate('/update-product');
           }



           useEffect(() => {
               // fetch products from the API and set them to the products state
               fetch(`${process.env.REACT_APP_API_URL}/products`)
                 .then(res => res.json())
                 .then(data => setProducts(data))
                 .catch(err => console.log(err));
             }, []);

             const handleDeactivateProduct = (productId) => {
               setSelectedProductId(productId);
  
               fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
                 method: 'PUT',
                 headers: {
                   'Content-Type': 'application/json',
                   Authorization: `Bearer ${localStorage.getItem('token')}`,
                 },
               })
                 .then(res => res.json())
                 .then(data => {
                   if (data !== null) {
                     Swal.fire({
                       title: "Product deactivated successfully",
                       icon: "success",
                       text: "The product has been deactivated.",
                     });
                    
                     setProducts(prevProducts => prevProducts.map(product => {
                       if (product._id === productId) {
                         product.isActive = false;
                       }
                       return product;
                     }));
                   } else {
                     Swal.fire({
                       title: "Error deactivating product",
                       icon: "error",
                       text: "There was an error deactivating the product. Please try again.",
                     });
                   }
                 })
               }   

             const handleReactivateProduct = (productId) => {
             setSelectedProductId(productId);

               fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
                          method: 'PUT',
                          headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`,
                          },
                          body: JSON.stringify({isActive: true})
                        })
                          .then(res => res.json())
                          .then(data => {
                            if (data !== null) {
                              Swal.fire({
                                title: "Product reactivated successfully",
                                icon: "success",
                                text: "The product has been reactivated.",
                              });
                             
                              setProducts(prevProducts => prevProducts.map(product => {
                                if (product._id === productId) {
                                  product.isActive = true;
                                }
                                return product;
                              }));
                            } else {
                              Swal.fire({
                                title: "Error reactivating product",
                                icon: "error",
                                text: "There was an error reactivating the product. Please try again.",
                              });
                            }
                          })
                        }  

        




	return (

<div id="bg-AD">
  <div className="container-fluid text-center">
	<h1>Admin Dashboard</h1>
   </div>
   	<div className="container-fluid text-center">	
	  <Link to="/create-product">
        <Button variant="primary">Create Product</Button>
      </Link>
		<Button className="m-2" variant="info" onClick={activeProduct}>Retrieve All active Products</Button>
	</div>	
	<Table id="bg-tableDB">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Price</th>
				<th>Availability</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			{
	    products.map((product, index) => (
	        <tr key={product._id}>
	            <td>{index + 1}</td>
	            <td>{product.title}</td>
	            <td>{product.description}</td>
	            <td>{product.price}</td>
	            <td>{product.isActive ? "Yes" : "No"}</td>
	           	<div>
	            <Button classname="bg-info" onClick={() => updateProduct(product._id)}>Update</Button>
	            </div>
	            
	           {product._id === selectedProductId && product.isActive === true ? (
                              <button className="bg-warning" onClick={() => handleDeactivateProduct(product._id)}>Deactivate</button>
                           ) :  ( 
                             <Button className="bg-danger" onClick={() => handleReactivateProduct(product._id)}>Reactivate</Button>
                           )}
	          				
	        </tr>
	    ))
			}
		</tbody>
	</Table>
</div>
	)
}
  