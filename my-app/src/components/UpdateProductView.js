import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";

import UserContext from "../UserContext";
import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import { Link } from 'react-router-dom'



export default function UpdateProductView (){

	const [newData, setNewData] = useState({
    title: "",
    description: "",
    price: 0,
  	});

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);
	const [productId, setProductId] = useState("");



	const updateProduct = (productId) => {
	  const newData = {
	    title: title,
	    description: description,
	    price: price,
	    isActive: isActive
	  };

	  if (user.isAdmin) {
	    fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${productId}`, {
	      method: "PUT",
	      headers: {
	        "Content-Type": "application/json",
	        Authorization: `Bearer ${localStorage.getItem("token")}`
	      },
	      body: JSON.stringify(newData)
	    })
	      .then((res) => res.json())
	      .then((data) => {
	        console.log(data);
	        if (newData !== null) {
	          Swal.fire({
	            title: "Product updated successfully",
	            icon: "success",
	            text: "Your product has been updated.",
	          });
	          navigate("/products");
	        } else {
	          Swal.fire({
	            title: "Error updating product",
	            icon: "error",
	            text: "There was an error updating your product. Please try again.",
	          });
	        }
	      });
	  } 
	};      



	return(

		(user.isAdmin === false || user.id === null) ?
		 <Navigate to ="/login" /> 
		 :
		<div className="col-md-6">
		      <h1>Update Product</h1>
		      <Form>     
		         <Form.Group>
		             <Form.Label>Title</Form.Label>
		             <Form.Control
		             type="text"
		             placeholder="Enter product title"
		             value={newData.title}
		             onChange={e => setNewData({newData, title: e.target.value})}
		             />
		         </Form.Group>
		         <Form.Group>
		             <Form.Label>Description</Form.Label>
		             <Form.Control
		             type="text"
		             placeholder="Enter product description"
		             value={newData.description}
		             onChange={e => setNewData({newData, description: e.target.value})}
		             />
		         </Form.Group>
		         <Form.Group>
		             <Form.Label>Price</Form.Label>
		             <Form.Control
		             type="number"
		             placeholder="Enter product price"
		             value={newData.price}
		             onChange={e => setNewData({newData, price: e.target.value})}
		             />
		         </Form.Group>
		            <div>
		             	<Button variant="primary" onClick={() => updateProduct(productId, newData)}>Update Product</Button>
		            </div>
		       </Form>
		</div>
		
	)
}