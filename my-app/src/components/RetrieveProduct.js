import React, { useState } from 'react';
import { Form, FormControl, Card } from 'react-bootstrap';



export default function RetrieveProduct  ({ products, setFilteredProducts }){
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = e => {
    setSearchTerm(e.target.value);
    const searchResults = products.filter(product =>
      product.title.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setFilteredProducts(searchResults);
  }

  return (
  	<Card>
   
    <div>
		<RetrieveProduct products={products} setFilteredProducts={setFilteredProducts} />
      <FormControl
        type="text"
        placeholder="Enter a product title"
        value={searchTerm}
        onChange={handleSearch}
      />
     </div> 
   
    </Card>
  );
};
