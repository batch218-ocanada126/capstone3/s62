import {useState, useEffect, useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import UserContext from '../UserContext';



export default function AppNavbar() {

  // State to store user information upon user login.
  //const [user, setUser] = useState(localStorage.getItem('email'));

  // State to store user information upon login
  const { user } = useContext(UserContext);

  const [isAdmin, setIsAdmin] = useState({
    id: null,
    isAdmin: null

  });

  return (

    <Navbar id="bg-navbar" className="sticky-top" expand="lg">

        <Navbar.Brand as={Link} to="/">CraftyInk</Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">

            {/*
            "as" - property that allows components to be treated as if they are different component gaining access to its property and functionalities
            "to" - property is used in place of the "href" property for providing URL for the page
            */}
          
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>

            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
           
            {
              (user.id !== null)  ?
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            :
            <> {/*fragment is use if there are 2 or more return*/}
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            </> }


            
            
            {
            (user.isAdmin === true) ?
             <Nav.Link as={NavLink} to="/adminDashboard">AdminDashboard</Nav.Link> 
              :
              <div className="d-none">
               <Nav.Link as={NavLink} to="/adminDashboard">AdminDashboard</Nav.Link> 
              </div>
            }
            
          
          </Nav>
        </Navbar.Collapse>

    </Navbar>
  );
}